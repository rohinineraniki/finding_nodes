const tree = {
  name: "A",
  children: [
    {
      name: "B",
      children: [
        {
          name: "C",
          children: [
            {
              name: "D",
              children: [],
            },
            {
              name: "E",
              children: [],
            },
          ],
        },
        {
          name: "F",
          children: [],
        },
      ],
    },
    {
      name: "G",
      children: [],
    },
  ],
};
const isObject = (obj) => {
  if (obj === null) {
    return false;
  }
  return typeof obj === "object";
};
let count = 0;
const countNodes = function (inputObject) {
  for (let value in inputObject) {
    if (isObject(inputObject[value])) {
      countNodes(inputObject[value]);
    } else {
      if (value === "name") {
        count += 1;
      }
    }
  }
  return count;
};
const result = countNodes(tree);
console.log(result);
